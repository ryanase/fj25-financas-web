package br.com.caelum.financas.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.caelum.financas.modelo.Categoria;

@Stateless
public class CategoriaDao implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;
	
	//Busca pelo ID.
	public Categoria procura(Integer id){
		return this.manager.find(Categoria.class, id);
	}
	
	//Retorna todas as categorias.
	public List<Categoria> lista(){
		String jpql = "SELECT c FROM Categoria c";
		TypedQuery<Categoria> query = manager.createQuery(jpql, Categoria.class);
		return query.getResultList();
	}
}
