package br.com.caelum.financas.dao;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.caelum.financas.exeption.ValorInvalidoException;
import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.Movimentacao;
import br.com.caelum.financas.modelo.TipoMovimentacao;
import br.com.caelum.financas.modelo.ValorPorMesEAno;

@Stateless
public class MovimentacaoDao {
	@Inject
	EntityManager manager;

	public void adiciona(Movimentacao movimentacao) {
		this.manager.joinTransaction();
		this.manager.persist(movimentacao);
		
		if(movimentacao.getValor().compareTo(BigDecimal.ZERO) < 0){
			throw new ValorInvalidoException("Movimentação negativa");
		}
	}

	public Movimentacao busca(Integer id) {
		return this.manager.find(Movimentacao.class, id);
	}

	public List<Movimentacao> lista() {
		return this.manager.createQuery("select m from Movimentacao m", Movimentacao.class).getResultList();
	}

	public void remove(Movimentacao movimentacao) {
		this.manager.joinTransaction();
		Movimentacao movimentacaoParaRemover = this.manager.find(Movimentacao.class, movimentacao.getId());
		this.manager.remove(movimentacaoParaRemover);
	}
	
	public List<Movimentacao> listaTodasMovimentacoes(Conta conta){
		String jpql = "SELECT m FROM Movimentacao m " +
					  "WHERE m.conta = :conta order by m.valor desc";
		TypedQuery<Movimentacao> query = this.manager.createQuery(jpql, Movimentacao.class);
		query.setParameter("conta", conta);
		return query.getResultList();
	}
	
	public List<Movimentacao> listaPorValorETipo(BigDecimal valor, TipoMovimentacao tipoMovimentacao){
		String jpql = "SELECT m FROM Movimentacao m " +
					  "WHERE m.valor <= :valor AND m.tipoMovimentacao = :tipo";
		TypedQuery<Movimentacao> query = this.manager.createQuery(jpql, Movimentacao.class);
		query.setParameter("valor", valor);
		query.setParameter("tipo", tipoMovimentacao);
		query.setHint("org.hibernate.cacheable", "true");
		return query.getResultList(); 
	}
	
	public BigDecimal calculaTotalMovimentado(Conta conta, TipoMovimentacao tipoMovimentacao){
		String jpql = "SELECT SUM(m.valor) FROM Movimentacao m " +
					  "WHERE m.conta <= :conta AND m.tipoMovimentacao = :tipo";
		TypedQuery<BigDecimal> query = this.manager.createQuery(jpql, BigDecimal.class);
		query.setParameter("conta", conta);
		query.setParameter("tipo", tipoMovimentacao);
		return query.getSingleResult(); 
	}
	
	public List<Movimentacao> buscaTodasMovimentacoesDaConta(String titular){
		String jpql = "SELECT m FROM Movimentacao m " +
					  "WHERE m.conta.titular LIKE :titular";
		TypedQuery<Movimentacao> query = this.manager.createQuery(jpql, Movimentacao.class);
		query.setParameter("titular", "%"+titular+"%");
		return query.getResultList();
		
	}
	
	public List<ValorPorMesEAno> listaMesesComMovimentacoes(Conta conta, TipoMovimentacao tipoMovimentacao){
		String jpql = "SELECT new br.com.caelum.financas.modelo.ValorPorMesEAno"
					+ "(MONTH(m.data), YEAR(m.data), SUM(m.valor)) "
					+ "FROM Movimentacao m "
					+ "WHERE m.conta = :conta AND m.tipoMovimentacao = :tipo "
					+ "GROUP BY YEAR(m.data) || MONTH(m.data) "
					+ "ORDER BY SUM(m.valor) desc ";
		TypedQuery<ValorPorMesEAno> query = this.manager.createQuery(jpql, ValorPorMesEAno.class);
		query.setParameter("conta", conta);
		query.setParameter("tipo", tipoMovimentacao);
				
		return query.getResultList();
	}
	
	public List<Movimentacao> listaComCategorias(){
		String jpql = "SELECT DISTINCT(m) FROM Movimentacao m "
				    + "LEFT JOIN FETCH m.categorias";
		TypedQuery<Movimentacao> query = manager.createQuery(jpql, Movimentacao.class);
		return query.getResultList();
	}
	
	public List<Movimentacao> listaTodasComCriteria(){
		CriteriaBuilder builder = this.manager.getCriteriaBuilder();
		CriteriaQuery<Movimentacao> criteria = builder.createQuery(Movimentacao.class);
		criteria.from(Movimentacao.class);
		return this.manager.createQuery(criteria).getResultList();
	}
	
	public BigDecimal somaMovimentacoesDoTiTtular(String titular){
		CriteriaBuilder builder = this.manager.getCriteriaBuilder();
		CriteriaQuery<BigDecimal> criteria = builder.createQuery(BigDecimal.class);
		Root<Movimentacao> root = criteria.from(Movimentacao.class);
		
		//no JPQL: SELECT SUM(m.valor)
		criteria.select(builder.sum(root.<BigDecimal>get("valor")));
		//no JPQL: WHERE m.conta.titular like :titular
		criteria.where(builder.like(root.<Conta>get("conta").<String>get("titular"), "%" + titular + "%"));

		return this.manager.createQuery(criteria).getSingleResult();
	}
	
	public List<Movimentacao> pesquisa(Conta conta, TipoMovimentacao tipoMovimentacao, Integer mes){
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Movimentacao> criteria = builder.createQuery(Movimentacao.class);
		Root<Movimentacao> root = criteria.from(Movimentacao.class);
		
		Predicate where = builder.conjunction();
		if (conta.getId() != null){
			where = builder.and(where, builder.equal(root.<Conta>get("conta"), conta));
		}
		if (tipoMovimentacao != null){
			where = builder.and(where, builder.equal(root.<TipoMovimentacao>get("tipoMovimentacao"), tipoMovimentacao));
		}
		if (mes != null && mes != 0){
			Expression<Integer> expression = builder.function("month", Integer.class, root.<Calendar>get("data"));
			where = builder.and(where, builder.equal(expression, mes));
		}
		
		criteria.where(where);
		return this.manager.createQuery(criteria).getResultList();
	}
}
