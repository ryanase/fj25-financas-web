package br.com.caelum.financas.service;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

//@Stateless
//@Startup
@Singleton
public class Agendador {

	private static int totalCriado;
	
	@Resource
	private TimerService timerservice;
	
	@PostConstruct
	void posConstrucao(){
		System.out.println("criando Agendador");
		totalCriado++;
	}
	
	@PreDestroy
	void preDestruicao(){
		System.out.println("destruindo agendador");
	}
	@AccessTimeout(unit = TimeUnit.SECONDS, value = 20)
	public void executa() {
		System.out.printf("%d instancia(s) criada(s) %n", totalCriado);

		// simulando demora de 4s na execucao
		try {
			System.out.printf("Executando %s %n", this);
			Thread.sleep(4000);
		} catch (InterruptedException e) {
		}
	}
	
	public void agenda(String expressaoMinutos, String expressaoSegundos){
		ScheduleExpression expression = new ScheduleExpression();
		expression.hour("*");
		expression.minute(expressaoMinutos);
		expression.second(expressaoSegundos);
		
		TimerConfig config = new TimerConfig();
		//config.setInfo(expression.toString());
		config.setInfo("Teste Timer");
		config.setPersistent(false);
		
		this.timerservice.createCalendarTimer(expression, config);
		System.out.println("Agendamento: " + expression);
	}
	
	@Timeout
	public void verificacaoPeriodicaSehaNovasContas(Timer timer){
		System.out.println(timer.getInfo());
		//aqui poderiamos acessar o banco de dados
		//com JPA para verificar as contas periodicamente
	}
	
	//@Schedule(hour="*", minute="*/1", second="0", persistent=false)
	public void enviaEmailCadaMinutoComInformacoesDasUltimaMovimentacoes(){
		System.out.println("Enviando email a cada minuto");
	}
}
